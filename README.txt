Versi Golang = go1.14.5
FrameWork = gin
Platform API = Postman
Database = MySQL
File SQL Database = user_gin.sql (file sql)
Database name = user_gin
Database username = root
Database password = ""

How to use:
-----------
Buat Database di MySQL dengan nama user_gin, dan import file SQL (user_gin) pada database 

Installation:
-------------
go get github.com/go-sql-driver/mysql
go get github.com/gin-gonic/gin
go get github.com/jinzhu/gorm
