//main.go
package main

import (
	"fmt"
	"gin_user/Config"
	"gin_user/Routes"

	"github.com/jinzhu/gorm"
)

var err error

func main() {
	Config.DB, err = gorm.Open("mysql", Config.DbURL(Config.BuildDBConfig()))
	if err != nil {
		fmt.Println("Status:", err)
	}

	r := Routes.EndPoint()
	//running
	r.Run()
}
