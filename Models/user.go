package Models

type User struct {
	Id      int    `json:"id" validate:"required" sql:"AUTO_INCREMENT"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
}
